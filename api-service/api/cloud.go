package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// CloudAPIRouter establishes endpoints to interact with the cloud-service
func CloudAPIRouter(router *mux.Router) {
	router.HandleFunc("/clouds", getClouds).Methods("GET")
	router.HandleFunc("/clouds", createCloud).Methods("POST")
	router.HandleFunc("/clouds/{cloud_ksuid}", getCloud).Methods("GET")
	router.HandleFunc("/clouds/{cloud_ksuid}", updateCloud).Methods("PUT")
	router.HandleFunc("/clouds/{cloud_ksuid}", deleteCloud).Methods("DELETE")
	router.HandleFunc("/clouds/{cloud_ksuid}/images", getImages).Methods("GET")
	router.HandleFunc("/clouds/{cloud_ksuid}/flavors", getFlavors).Methods("GET")
}

// Gets an array of cloud providers
func getClouds(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /clouds triggered")
}

// Create a new cloud provider
func createCloud(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /clouds triggered")
}

// Gets cloud by cloud_ksuid
func getCloud(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /clouds/{cloud_ksuid} triggered")
}

// Modify cloud
func updateCloud(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PUT /clouds/{cloud_ksuid} triggered")
}

// Delete a cloud
func deleteCloud(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /clouds/{cloud_ksuid} triggered")
}

// Gets an array of images from a cloud provider. This method is only available from instance-type cloud providers.
func getImages(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /clouds/{cloud_ksuid}/images triggered")
}

// Gets an array of flavors from a cloud provider. This method is only available from instance-type cloud providers.
func getFlavors(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /clouds/{cloud_ksuid}/flavors triggered")
}
