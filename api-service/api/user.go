package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// UserAPIRouter establishes endpoints to interact with the user-service
func UserAPIRouter(router *mux.Router) {
	router.HandleFunc("/users", getUsers).Methods("GET")
	router.HandleFunc("/users", createUser).Methods("POST")
	router.HandleFunc("/users/{username}", getUser).Methods("GET")
	router.HandleFunc("/users/{username}", updateUser).Methods("PUT")
	router.HandleFunc("/users/{username}", deleteUser).Methods("DELETE")
}

// Gets an array of users. Non-administrative users will only get one user, self
func getUsers(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /users triggered")
}

// Create a new user
func createUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /users triggered")
}

// Get existing user information
func getUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /user/{username} triggered")
}

// Update existing user; admins can modify any user, but developers can only change their own user
func updateUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PUT /user/{username} triggered")
}

// Delete an existing user
func deleteUser(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /user/{username} triggered")
}
