package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// TemplateAPIRouter establishes endpoints to interact with the template-service
func TemplateAPIRouter(router *mux.Router) {
	router.HandleFunc("/templates", getTemplates).Methods("GET")
	router.HandleFunc("/templates", createTemplate).Methods("POST")
	router.HandleFunc("/templates/{template_ksuid}", getTemplate).Methods("GET")
	router.HandleFunc("/templates/{template_ksuid}", updateTemplate).Methods("PUT")
	router.HandleFunc("/templates/{template_ksuid}", deleteTemplate).Methods("DELETE")

}

// Gets an array of templates
func getTemplates(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /templates triggered")
}

// Create a new template
func createTemplate(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /templates triggered")
}

// Gets template by template_ksuid
func getTemplate(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /templates/{template_ksuid} triggered")
}

// Modify template
func updateTemplate(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PUT /templates/{template_ksuid} triggered")
}

// Delete a template
func deleteTemplate(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /templates/{template_ksuid} triggered")
}
