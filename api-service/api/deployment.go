package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// DeploymentAPIRouter establishes endpoints to interact with the deployment-service
func DeploymentAPIRouter(router *mux.Router) {
	// Deployment
	router.HandleFunc("/deployments", getDeployments).Methods("GET")
	router.HandleFunc("/deployments", createDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_ksuid}", getDeployment).Methods("GET")
	router.HandleFunc("/deployments/{deployment_ksuid}", updateDeployment).Methods("PUT")
	router.HandleFunc("/deployments/{deployment_ksuid}", deleteDeployment).Methods("DELETE")

	// Deployment Builds
	router.HandleFunc("/deployments/{deployment_ksuid}/builds", getBuilds).Methods("GET")
	router.HandleFunc("/deployments/{deployment_ksuid}/builds", rebuildDeployment).Methods("POST")
	router.HandleFunc("/deployments/{deployment_ksuid}/builds/latest", getLatestBuild).Methods("GET")
	router.HandleFunc("/deployments/{deployment_ksuid}/builds/{build_skuid}", getBuild).Methods("GET")

	// Deployment Runs
	router.HandleFunc("/deployments/{deployment_ksuid}/runs", getRuns).Methods("GET")
	router.HandleFunc("/deployments/{deployment_ksuid}/runs", createRun).Methods("POST")
	router.HandleFunc("/deployments/{deployment_ksuid}/runs/latest", getLatestRun).Methods("GET")
	router.HandleFunc("/deployments/{deployment_ksuid}/runs/{run_skuid}", getRun).Methods("GET")

	// Deployment Resources
	router.HandleFunc("/deployments/{deployment_ksuid}/resources", getResources).Methods("GET")

	// Deployment volume actions (attach/detach)
	router.HandleFunc("/deployments/{deployment_ksuid}/volume_attach/{volume_ksuid}", attachVolume).Methods("POST")
	router.HandleFunc("/deployments/{deployment_ksuid}/volume_detach/{volume_ksuid}", detachVolume).Methods("POST")

	// Volume
	router.HandleFunc("/volumes", getVolumes).Methods("GET")
	router.HandleFunc("/volumes", createVolume).Methods("POST")
	router.HandleFunc("/volumes/{volumes_ksuid}", getVolume).Methods("GET")
	router.HandleFunc("/volumes/{volumes_ksuid}", updateVolume).Methods("PUT")
	router.HandleFunc("/volumes/{volumes_ksuid}", deleteVolume).Methods("DELETE")
}

// Gets an array of deployments
func getDeployments(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments triggered")
}

// Create a new deployment
func createDeployment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /deployments triggered")
}

// Gets deployment by deployment_ksuid
func getDeployment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid} triggered")
}

// Modify deployment
func updateDeployment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("UPDATE /deployments/{deployment_ksuid} triggered")
}

// Delete a deployment
func deleteDeployment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /deployments/{deployment_ksuid} triggered")
}

// Gets an array of builds for a deployment
func getBuilds(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/builds triggered")
}

// Rebuild a deployment
func rebuildDeployment(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /deployments/{deployment_ksuid}/builds triggered")
}

// Gets information about the latest build
func getLatestBuild(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/builds/latest triggered")
}

// Gets information about a build by build_skuid
func getBuild(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/builds/{build_skuid} triggered")
}

// Gets an array of runs for a deployment
func getRuns(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/runs triggered")
}

// Create a new run
func createRun(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /deployments/{deployment_ksuid}/runs triggered")
}

// Gets information about the latest run
func getLatestRun(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/runs/latest triggered")
}

// Gets information about the run by skuid
func getRun(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/runs/{run_skuid} triggered")
}

// Gets an array of resources that belong to the deployment
func getResources(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /deployments/{deployment_ksuid}/resources triggered")
}

// Attaches a volume type (deployment) to an existing deployment
func attachVolume(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /deployments/{deployment_ksuid}/volume_detach/{volume_ksuid} triggered")
}

// Detaches a volume type (deployment) from an existing deployment
func detachVolume(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /deployments/{deployment_ksuid}/volume_detach/{volume_ksuid} triggered")
}

// Gets an array of volumes
func getVolumes(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /volumes triggered")
}

// Create a new volume. This is a convenience method that creates a deployment of type volume.
func createVolume(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /volumes triggered")
}

// Gets volume by volume_ksuid
func getVolume(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /volumes/{volumes_ksuid} triggered")
}

// Modify volume. This is a convenience method that modifies a deployment of type volume.
func updateVolume(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PUT /volumes/{volumes_ksuid} triggered")
}

// Delete a volume
func deleteVolume(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /volumes/{volumes_ksuid} triggered")
}
