package api

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"golang.org/x/oauth2"
)

var (
	tokenStore = map[string]string{}
)

// AuthAPIRouter will map the necessary auth functions to the endpoints
func AuthAPIRouter(router *mux.Router) {
	router.HandleFunc("/user/login", keycloakLogin).Methods("GET")
	router.HandleFunc("/user/login", keycloakPostLogin).Methods("POST")
	router.HandleFunc("/user/login/callback", keycloakCallback)
}

func keycloakPostLogin(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "keycloakPostLogin",
	})

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read request body")
		jsonError(w, "unable to read request body: "+err.Error(), http.StatusBadRequest)
		return
	}
	reqBodyStr := string(reqBody) + fmt.Sprintf(
		"&grant_type=password&client_id=%s&client_secret=%s&scope=openid",
		keycloakClient.OAuthConfig.ClientID,
		keycloakClient.OAuthConfig.ClientSecret,
	)
	req, err := http.NewRequest("POST", keycloakClient.URL+"/protocol/openid-connect/token", nil)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to create Keycloak POST request")
		jsonError(w, "unable to create Keycloak POST request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	req.Body = ioutil.NopCloser(bytes.NewReader([]byte(reqBodyStr)))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to execute Keycloak POST request")
		jsonError(w, "unable to execute Keycloak POST request: "+err.Error(), http.StatusInternalServerError)
		return
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read Keycloak response")
		jsonError(w, "unable to read Keycloak response: "+err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(respBody)
}

// keycloakLogin will redirect to the Keycloak login page with an HMAC state string
// saved in a cookie
func keycloakLogin(w http.ResponseWriter, r *http.Request) {
	// First create a cookie and add it to the response
	cookie, err := r.Cookie("koa_csrf")
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "api",
			"function": "keycloakLogin",
		}).Warn("no existing cookie, creating a new one")
		cookie = &http.Cookie{Name: "koa_csrf", Value: xid.New().String()}
	}
	http.SetCookie(w, cookie)
	// Now append a secret and hash the cookie value
	mac := hmac.New(sha256.New, hmacSecret)
	mac.Write([]byte(cookie.Value))
	state := hex.EncodeToString(mac.Sum(nil))
	http.Redirect(w, r, keycloakClient.OAuthConfig.AuthCodeURL(state), http.StatusFound)
}

// keycloakCallback will respond to a callback from the Keycloak server in order
// to exchange the code for an access token that will be displayed to the user
func keycloakCallback(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "keycloakCallback",
	})

	// Read cookie from request
	cookie, err := r.Cookie("koa_csrf")
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to get CSRF token")
		jsonError(w, "unable to get CSRF token", http.StatusBadRequest)
		return
	}
	// Calculate expected state using cookie
	mac := hmac.New(sha256.New, hmacSecret)
	mac.Write([]byte(cookie.Value))
	expectedState := hex.EncodeToString(mac.Sum(nil))
	// Make sure states match
	if !hmac.Equal([]byte(r.URL.Query().Get("state")), []byte(expectedState)) {
		logger.WithFields(log.Fields{
			"state":          r.URL.Query().Get("state"),
			"expected_state": expectedState,
		}).Error("state did not match")
		jsonError(w, "state did not match", http.StatusBadRequest)
		return
	}

	oauth2Token, err := keycloakClient.OAuthConfig.Exchange(context.Background(), r.URL.Query().Get("code"))
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("failed to exchange token")
		jsonError(w, "failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}
	rawIDToken, ok := oauth2Token.Extra("id_token").(string)
	if !ok {
		logger.WithFields(log.Fields{"error": fmt.Errorf("key 'id_token' does not exist")}).Error("unable to get id_token from OAuth2 token")
		jsonError(w, "unable to get id_token from OAuth2 token", http.StatusInternalServerError)
		return
	}

	idToken, err := keycloakClient.Verify(rawIDToken)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to verify ID token")
		jsonError(w, "unable to verify ID token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	resp := struct {
		OAuth2Token   *oauth2.Token
		IDTokenClaims *json.RawMessage
		IDToken       string
	}{oauth2Token, new(json.RawMessage), rawIDToken}

	err = idToken.Claims(&resp.IDTokenClaims)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to read ID token claims")
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	data, err := json.Marshal(resp)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Error("unable to marshal JSON response")
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.Write(data)
}

// AuthenticationMiddleware is a middleware function required on some routes to
// make sure a user is authenticated and authorizaed to access that endpoint
func AuthenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		logger := log.WithFields(log.Fields{
			"package":  "api",
			"function": "AuthenticationMiddleware",
		})

		_, username, admin, err := authorize(r.Header, w)
		if err != nil {
			logger.WithFields(log.Fields{"error": err}).Error("unable to authenticate/authorize request")
			jsonError(w, err.Error(), http.StatusUnauthorized)
			return
		}

		r.Header.Add("X-Koa-User", username)
		r.Header.Add("X-Koa-Admin", fmt.Sprintf("%t", admin))
		next.ServeHTTP(w, r)
	})
}

// Authorize is a helper to read the token header and verify it against Keycloak.
// It will also get the username from the token
func authorize(header http.Header, w http.ResponseWriter) (accessToken string, username string, admin bool, err error) {
	accessToken = header.Get("Authorization")

	// Otherwise, cointinue with Keycloak login
	idToken, err := keycloakClient.Verify(accessToken)
	if err != nil {
		err = fmt.Errorf("unable to verify access token: %v", err)
		jsonError(w, err.Error(), http.StatusForbidden)
		return
	}
	// Get username from idToken
	idTokenClaims := struct {
		Username string `json:"preferred_username"`
		KoaAdmin string `json:"koa_admin"`
	}{}
	err = idToken.Claims(&idTokenClaims)
	if err != nil {
		err = fmt.Errorf("unable to read ID token claims: %v", err)
		jsonError(w, err.Error(), http.StatusInternalServerError)
		return
	}
	username = idTokenClaims.Username
	admin = (idTokenClaims.KoaAdmin == "[koa_admin]")
	return
}
