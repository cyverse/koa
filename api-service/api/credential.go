package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// CredentialsAPIRouter establishes endpoints to interact with the credential-service
func CredentialsAPIRouter(router *mux.Router) {
	// Credentials
	router.HandleFunc("/credentials", getCredentials).Methods("GET")
	router.HandleFunc("/credentials", createCredential).Methods("POST")
	router.HandleFunc("/credentials/{credential_ksuid}", getCredential).Methods("GET")
	router.HandleFunc("/credentials/{credential_ksuid}", updateCredential).Methods("PUT")
	router.HandleFunc("/credentials/{credential_ksuid}", deleteCredential).Methods("DELETE")

	// Credential Types
	router.HandleFunc("/credential_types", getCredentialTypes).Methods("GET")
}

// Gets an array of credentials
func getCredentials(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /credentials triggered")
}

// Create a new credential
func createCredential(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /credentials triggered")
}

// Gets credential by credential_ksuid
func getCredential(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /credentials/{credential_ksuid} triggered")
}

// Modify credential
func updateCredential(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PUT /credentials/{credential_ksuid} triggered")
}

// Delete a credential
func deleteCredential(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /credentials/{credential_ksuid} triggered")
}

// Gets a list of credential types
func getCredentialTypes(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /credential_types triggered")
}
