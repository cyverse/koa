package api

import (
	"os"

	"gitlab.com/cyverse/koa/api-service/api/keycloak"
)

var (
	keycloakClient *keycloak.Client
	hmacSecret     []byte
)

// InitKeycloakClient is used to define Keycloak connection variables
func InitKeycloakClient(defaultURL, defaultRedirectURL, defaultClientID string) (err error) {
	address := os.Getenv("KEYCLOAK_URL")
	if len(address) == 0 {
		address = defaultURL
	}
	redirect := os.Getenv("KEYCLOAK_REDIRECT_URL")
	if len(redirect) == 0 {
		redirect = defaultRedirectURL
	}
	clientID := os.Getenv("KEYCLOAK_ID")
	if len(clientID) == 0 {
		clientID = defaultClientID
	}

	keycloakClient, err = keycloak.NewClient(
		address,
		redirect,
		clientID,
		os.Getenv("KEYCLOAK_SECRET"),
	)

	hmacSecret := []byte(os.Getenv("HMAC_SECRET"))
	if len(hmacSecret) == 0 {
		hmacSecret = []byte("secret")
	}
	return
}
