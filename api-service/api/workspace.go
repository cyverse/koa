package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

// WorkspaceAPIRouter establishes endpoints to interact with the (TBD)-service
func WorkspaceAPIRouter(router *mux.Router) {
	router.HandleFunc("/workspaces", getWorkspaces).Methods("GET")
	router.HandleFunc("/workspaces", createWorkspace).Methods("POST")
	router.HandleFunc("/workspaces/{workspace_ksuid}", getWorkspace).Methods("GET")
	router.HandleFunc("/workspaces/{workspace_ksuid}", updateWorkspace).Methods("PUT")
	router.HandleFunc("/workspaces/{workspace_ksuid}", deleteWorkspace).Methods("DELETE")
}

// Gets an array of workspaces
func getWorkspaces(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /workspaces triggered")
}

// Creates a new workspace
func createWorkspace(w http.ResponseWriter, r *http.Request) {
	fmt.Println("POST /workspaces triggered")
}

// Gets workspace by workspace_ksuid
func getWorkspace(w http.ResponseWriter, r *http.Request) {
	fmt.Println("GET /workspaces/{workspace_ksuid} triggered")
}

// Modify workspace
func updateWorkspace(w http.ResponseWriter, r *http.Request) {
	fmt.Println("PUT /workspaces/{workspace_ksuid} triggered")
}

// Delete a workspace
func deleteWorkspace(w http.ResponseWriter, r *http.Request) {
	fmt.Println("DELETE /workspaces/{workspace_ksuid} triggered")
}
