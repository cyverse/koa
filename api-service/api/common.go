package api

import (
	"fmt"
	"net/http"
)

// jsonError will create and write a nice JSON error response
func jsonError(w http.ResponseWriter, msg string, code int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write([]byte(fmt.Sprintf(`{"error": {"code": %d, "message": "%s"}}`, code, msg)))
}
