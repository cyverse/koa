package main

import (
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
	"gitlab.com/cyverse/koa/api-service/api"
)

const (
	defaultKeycloakURL         = "http://10.244.0.10:8080/auth/realms/master"
	defaultKeycloakRedirectURL = "http://localhost:8000/user/login/callback"
	defaultKeycloakClientID    = "koa"
)

func init() {
	// Configure logger
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		level = log.InfoLevel
	}
	log.SetLevel(level)

	logger := log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	})

	err = api.InitKeycloakClient(
		defaultKeycloakURL,
		defaultKeycloakRedirectURL,
		defaultKeycloakClientID,
	)
	if err != nil {
		logger.WithFields(log.Fields{"error": err}).Panic("unable to connect to Keycloak")
		panic(err)
	}
	logger.Info("initialized connection to Keycloak")

}

func main() {
	router := setupRouter()

	log.WithFields(log.Fields{
		"package":  "main",
		"function": "init",
	}).Info("API Service listening on port 8000")
	http.ListenAndServe(":8000", router)
}

func setupRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	api.AuthAPIRouter(router)

	subRouter := router.NewRoute().Subrouter()
	subRouter.Use(api.AuthenticationMiddleware)

	// Setup paths for all services
	api.UserAPIRouter(subRouter)
	api.WorkspaceAPIRouter(subRouter)
	api.DeploymentAPIRouter(subRouter)
	api.CloudAPIRouter(subRouter)
	api.TemplateAPIRouter(subRouter)
	api.AccountingAPIRouter(subRouter)

	return router
}
